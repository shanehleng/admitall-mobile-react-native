import React from 'react';
import { 
  StyleSheet, 
  View, 
  Text, 
  ImageBackground, 
  Image,
  Dimensions
} from 'react-native';

import AppIntroSlider from 'react-native-app-intro-slider';

const { width: WIDTH } = Dimensions.get('window');

const slides = [
  {
    key: 'somethun',
    title: 'Search',
    description: 'Browse our list of local colleges and universities to find the school and academic program that fits your career goals and aspirations.',
    bgImage: require('../assets/images/searchbg.png'),
    iconImage: require('../assets/images/icon_walkthrough_search.png'),
  },
  {
    key: 'somethun1',
    title: 'Apply',
    description: 'Fill out the online application form and upload any supporting documents to get the application process started in your program of choice.',
    bgImage: require('../assets/images/applybg.png'),
    iconImage: require('../assets/images/icon_walkthrough_apply.png'),
  },
  {
    key: 'somethun2',
    title: 'Save',
    description: 'Secure your enrollment in your desired program and save up to 50% on tuition fees by availing of AdmitAll partner university promotions and AdmitAll gift checks.',
    bgImage: require('../assets/images/savebg.png'),
    iconImage: require('../assets/images/icon_walkthrough_save.png'),
  },
];

export default class AppIntro extends React.Component {
  _renderItem = props => {
    return (
      <ImageBackground
        style={[styles.background, { paddingTop: props.topSpacer,
          paddingBottom: props.bottomSpacer,
          width: props.width,
          height: props.height, 
        }]}
        source={props.bgImage}
      >
        <Image source={props.iconImage} style={styles.bgIcons} />
        <View>
          <Text style={styles.title}>{props.title}</Text>
          <Text style={styles.description}>{props.description}</Text>
        </View>
      </ImageBackground>
    );
  }

  _renderNextButton = props => {
    return (
      <View style={styles.button}>
        <Text style={styles.buttonText}>Next</Text>
      </View>
    );
  }

  _renderDoneButton = props => {
    return (
      <View style={styles.button}>
        <Text style={styles.buttonText}>Start</Text>
      </View>
    );
  }

  _renderSkipButton = props => {
    return (
      <View style={styles.skip}>
        <Text style={styles.skip}>Skip</Text>
      </View>
    );
  }

  render() {
    return (
      <AppIntroSlider
        slides={slides}
        renderItem={this._renderItem}
        renderNextButton={this._renderNextButton}
        renderDoneButton={this._renderDoneButton}
        renderSkipButton={this._renderSkipButton}
        dotStyle={styles.navDot}
        activeDotStyle={styles.activenavDot}
        onDone={() => this.props.navigation.navigate('LoginScreen')}
        showSkipButton
      />
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  title: {
    marginTop: 15,
    fontSize: 20,
    color: '#40b4e4',
    textAlign: 'center',
    fontFamily: 'robotoregular',
  },  
  description: {
    fontFamily: 'robotolight',
    fontSize: 13,
    lineHeight: 20,
    color: '#99a5b1',
    textAlign: 'center',
    paddingHorizontal: 40,
    marginTop: 15,
  },
  bgIcons: {
    width: 120,
    height: 120,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 150,
  },
  navDot: {
    backgroundColor: '#f0f0f0',
    width: 7,
    height: 7,
    bottom: 5,
  },
  activenavDot: {
    backgroundColor: '#40b4e4',
    width: 7,
    height: 7,
    bottom: 5,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center', 
    marginLeft: (WIDTH - 230) / 2,
    marginRight: (WIDTH - 230) / 2,
    marginBottom: 110,
  },
  buttonText: {
    width: 200,
    height: 40,
    backgroundColor: 'transparent',
    borderColor: '#40b4e4',
    borderWidth: 1,
    color: '#40b4e4',
    textAlign: 'center',
    fontFamily: 'robotolight',
    fontSize: 14,
    lineHeight: 40,
  },
  skip: {
    color: '#40b4e4',
    fontSize: 11,
    fontFamily: 'robotolight',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: -10,
    marginLeft: (WIDTH - 230) / 2,
    marginRight: (WIDTH - 230) / 2,
  }
});