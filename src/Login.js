
import React, { Component } from 'react';
import {
  Platform, 
  StyleSheet,  
  View,
  Image,
  Text,
  ImageBackground,
  Dimensions,
  Icon,
  TextInput
} from 'react-native';

import loginBg from '../assets/images/loginbg.png';
import logo from '../assets/images/logo.png';

const { width: WIDTH } = Dimensions.get('window');


export default class Login extends React.Component {
    static navigationOptions = {
        header: null
    }

    render() {
        return (
            <ImageBackground source={loginBg} style={styles.container}>
                <View style="{styles.logoContainer}">
                    <Image source={logo} style={styles.logo} />
                </View>
                <View style={styles.inputContainer}>
            
                    <TextInput 
                        style={styles.input}
                        placeholder={'Email Address'}
                        placeholderTextColor={'#99a5b1'}
                        underlineColorAndroid='transparent'
                    />

                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logoContainer: {
        alignItems: 'center',
        marginBottom: 50,
        top: -100,
    },
    logo: {
        width: 230,
        height: 70,
    },
    inputContainer: {
        marginTop: 20
    },
    input: {
        width: WIDTH - 130,
        height: 45,
        fontSize: 16,
        paddingLeft: 45,
        backgroundColor: '#fff',
        borderColor: '#99a5b1',
        borderWidth: 0.5,
        color: '#99a5b1',
        marginHorizontal: 25,
        fontFamily: 'robotolight',
        fontSize: 14,
        elevation: 3,
    },
});


