
import React, { Component } from 'react';
import {
  Platform, 
  StyleSheet,  
  View,
  Image,
} from 'react-native';

import { StackNavigator } from 'react-navigation';

import * as Progress from 'react-native-progress';
import logo from '../assets/images/logo.png';

export default class Splash extends React.Component {
    static navigationOptions = {
        header: null
    }

    componentWillMount() {
        setInterval( () => {
            this.props.navigation.navigate('AppIntroScreen');
        }, 9000)
    }

    constructor(props) {
        super(props);

        this.state = {
            progress: 0
        };
    }

    componentDidMount() {
        this.animate();
    }

    animate() {
        let progress = 0;
        this.setState({ progress });
        setTimeout(() => {
            setInterval(() => {
                progress += Math.random() / 5;
                if (progress > 1) {
                progress = 1;
                }
                this.setState({ progress });
            }, 500);
        }, 1500);
    }

    // componentWillUnmount() {
    //     clearTimeout(this.setState);
    // }

    render() {
        return (
            <View style={styles.container}>
                <Image source={logo} style={styles.logo} />
                <Progress.Bar
                    style={styles.progress}
                    progress={this.state.progress}
                    width={235}
                    color={'#40b4e4'}
                    height={5}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingVertical: 20,
    },
    logo: {
        marginTop: -50,
        width: 250,
        height: 76,
    },
    progress: {
        marginTop: 15,
        borderRadius: 0,
        height: 4, 
        backgroundColor: '#f0f0f0',
        borderColor: 'transparent',
    },
});


