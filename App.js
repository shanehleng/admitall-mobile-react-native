
import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View
} from 'react-native';

import { StackNavigator } from 'react-navigation';
import Splash from './src/Splash';
import AppIntro from './src/AppIntro';
import Login from './src/Login';

const Navigation = StackNavigator({
  Home: {
    screen: Login,
    navigationOptions: {
      header: null,
      headerLeft: null,
      gesturesEnabled: false
    }
  },
  AppIntroScreen: {
    screen: AppIntro,
    navigationOptions: {
      header: null
    }
  },
  LoginScreen: {
    screen: Login,
  },
})

export default Navigation;
